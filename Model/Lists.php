<?php


namespace Model;

use HTTPStatusCodes;
use JsonResponse;
use System;

class Lists
{
    public function __construct()
    {
        $mysql = new MySQL();
        $mysql->create_table('lists', [
            new TableColumn('id', ColumnTypes::BIGINT, 20, true, null, true, true),
            new TableColumn('date', ColumnTypes::TIMESTAMP, 0, false, 'current_timestamp'),
            new TableColumn('id_usuario', ColumnTypes::BIGINT, 20, true),
            new TableColumn('filename', ColumnTypes::VARCHAR, 255, true),
            new TableColumn('ipa', ColumnTypes::VARCHAR, 100),
            new TableColumn('period_start', ColumnTypes::DATE),
            new TableColumn('period_end', ColumnTypes::DATE),
            new TableColumn('status', ColumnTypes::VARCHAR, 100, false, 'pending'),
        ], <<<sql
create unique index lists_ipa_period_start_period_end_uindex on lists (ipa, period_start, period_end);
sql
        );
    }

    public function saveFile($id_usuario, array $file)
    {
        $mysql = new MySQL();
        $sql = <<<sql
select id,filename,status from lists where ipa=? and period_start=? and period_end=? for update;
sql;
        $old_list = $mysql->fetch_single($mysql->prepare($sql, ['sss', $file['ipa'], $file['period_start'], $file['period_end']]));
        $old = System::isset_get($old_list);

        if ($old['id']) {
            if ($old['status'] === 'completed') {
                JsonResponse::sendResponse(['message' => 'This list is already imported.'], HTTPStatusCodes::BadRequest);
            }

            unlink($file['path'] . $old['filename']);
            $sql = <<<sql
update lists set date=?,id_usuario=?,filename=? where id=?
sql;
            $mysql->prepare($sql, ['sisi', date('Y-m-d H:i:s'), $id_usuario, $file['filename'], $old['id']]);
            return $old['id'];
        } else {
            $sql = <<<sql
insert into lists values(?,?,?,?,?,?,?,?);
sql;
            $mysql->prepare($sql, [
                'isisssss',
                null,//id
                null,//date
                $id_usuario,//id_usuario
                $file['filename'],//filename
                $file['ipa'],//ipa
                $file['period_start'],//period_start
                $file['period_end'],//period_end
                'pending',//status
            ]);
            return $mysql->insertID();
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function getList($id)
    {
        $sql = <<<sql
select * from lists where id=?
sql;
        $mysql = new MySQL();
        return $mysql->fetch_single($mysql->prepare($sql, ['i', $id]));
    }

    public function getPreviousList($period_start, $ipa)
    {
        $sql = <<<sql
select filename
from lists
where month(period_start) = month(?)-1 and ipa=?
order by period_start desc
limit 1;
sql;
        $mysql = new MySQL();
        return $mysql->fetch_single($mysql->prepare($sql, ['ss', $period_start, $ipa]));
    }

    public function setListStatus($id, $status)
    {
        $sql = <<<sql
update lists set status=? where id=?
sql;
        $mysql = new MySQL();
        $mysql->prepare($sql, ['si', $status, $id]);
    }

    public function updateStatus(int $id, string $status)
    {
        $sql = <<<sql
update lists set status=? where id=?;
sql;
        $mysql = new MySQL();
        $mysql->prepare($sql, ['si', $status, $id]);
    }

    public function deleteList($id)
    {
        $sql = <<<sql
delete from lists where id = ?;
sql;
        $mysql = new MySQL();
        $mysql->prepare($sql, ['i', $id]);
    }
}