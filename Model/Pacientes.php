<?php


namespace Model;


class Pacientes
{
    public function __construct()
    {
        $mysql = new MySQL();
        $mysql->create_table('pacientes', [
            new TableColumn('id', ColumnTypes::BIGINT, 20, true, null, true, true),
            new TableColumn('id_usuario', ColumnTypes::BIGINT, 20, true),
            new TableColumn('id_list', ColumnTypes::BIGINT, 20, true),
            new TableColumn('fecha', ColumnTypes::TIMESTAMP, 0, false, 'current_timestamp'),
            new TableColumn('status', ColumnTypes::VARCHAR, 100),
            new TableColumn('ProviderLName', ColumnTypes::VARCHAR, 100),
            new TableColumn('ProviderNpi', ColumnTypes::VARCHAR, 100),
            new TableColumn('IPAName', ColumnTypes::VARCHAR, 100),
            new TableColumn('SubscriberNumber', ColumnTypes::VARCHAR, 100),
            new TableColumn('MemberLName', ColumnTypes::VARCHAR, 100),
            new TableColumn('MemberFName', ColumnTypes::VARCHAR, 100),
            new TableColumn('DateOfBirth', ColumnTypes::DATE, 0),
            new TableColumn('MemberPhone', ColumnTypes::VARCHAR, 100),
            new TableColumn('Gender', ColumnTypes::VARCHAR, 100),
            new TableColumn('MemberEffectiveDate', ColumnTypes::DATE, 0),
            new TableColumn('MemberAddress', ColumnTypes::VARCHAR, 100),
            new TableColumn('City', ColumnTypes::VARCHAR, 100),
            new TableColumn('State', ColumnTypes::VARCHAR, 100),
            new TableColumn('Zip', ColumnTypes::INTEGER, 100),
            new TableColumn('County', ColumnTypes::VARCHAR, 100),
            new TableColumn('Language', ColumnTypes::VARCHAR, 100),
        ]);

        new Lists();
        new Representatives();
    }

    function insertPaciente($id_usuario, $id_list, $paciente)
    {
        MySQL::default_values($paciente, ['id', 'status']);

        $sql = <<<sql
insert into pacientes values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);
sql;
        $mysql = new MySQL();
        $mysql->prepare($sql, [
            'iiisssssssssssssssiss',
            $paciente['id'],//id
            $id_usuario,//id_usuario
            $id_list,//id_list
            null,//fecha
            $paciente['status'],//status
            $paciente['ProviderLName'],//ProviderLName
            $paciente['ProviderNpi'],//ProviderNpi
            $paciente['IPAName'],//IPAName
            $paciente['SubscriberNumber'],//SubscriberNumber
            $paciente['MemberLName'],//MemberLName
            $paciente['MemberFName'],//MemberFName
            $paciente['DateOfBirth'],//DateOfBirth
            $paciente['MemberPhone'],//MemberPhone
            $paciente['Gender'],//Gender
            $paciente['MemberEffectiveDate'],//MemberEffectiveDate
            $paciente['MemberAddress'],//MemberAddress
            $paciente['City'],//City
            $paciente['State'],//State
            $paciente['Zip'],//Zip
            $paciente['County'],//County
            $paciente['Language'],//Language
        ]);
    }

    public function selectPaciente(array $patient)
    {
        $sql = <<<sql
select * from pacientes p
inner join lists l on month(period_start)<=month(?)
where SubscriberNumber=?;
sql;
        $mysql = new MySQL();
        return $mysql->fetch_single($mysql->prepare($sql, ['ss', $patient['period_start'], $patient['SubscriberNumber']]));
    }

    public function selectListTotals($period)
    {
        $sql = <<<sql
select id_list                                                    id_list,
       filename,
       date,
       ipa,
       l.period_start,
       period_end,
       name_representative                                        rep,
       l.status,
       sum(1)                                                     total,
       sum(if(p.status = 'new', 1, 0))                            new,
       sum(if(p.status <> 'new', 1, 0)) unchanged
from pacientes p
         left join lists l on p.id_list = l.id
         left join _usuarios u on l.id_usuario = u.id_usuario
         left join representatives r on u.id_rep = r.id_representative
#          join (select period_start from lists l2 where month(l2.period_start) = month('$period[start]') - 1) l2
where if('$period[start]' = '', true, l.period_start >= '$period[start]')
  and if('$period[end]' = '', true, period_end <= '$period[end]')
group by id_list
sql;
        $mysql = new MySQL();
        return $mysql->fetch_all($mysql->query($sql));
    }

    public function deletePatientsFromList($id)
    {
        $sql = <<<sql
delete from pacientes where id_list=?;
sql;
        $mysql = new MySQL();
        $mysql->prepare($sql, ['i', $id]);
    }

    public function selectSubscriberNumbers($period_start)
    {
        $sql = <<<sql
select SubscriberNumber from pacientes
inner join lists l on month(period_start)=month(?)-1;
sql;
        $mysql = new MySQL();
        return $mysql->fetch_all($mysql->prepare($sql, ['s', $period_start]));
    }

    public function insertPacientes(array $new_patients)
    {
        $pacientes = "('" . implode("'),('", $new_patients) . "')";
        $sql = <<<sql
insert into pacientes values $pacientes
sql;
        $mysql = new MySQL();
        $path = DIR . '/Data/insertPacientes.sql';
        file_put_contents($path, $sql);
        $mysql->query("use {$mysql->database()}; SOURCE Data/insertPacientes.sql");
        unlink($path);
    }
}