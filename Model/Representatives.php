<?php


namespace Model;


class Representatives
{
    public function __construct()
    {
        $mysql = new MySQL();
        $mysql->create_table('representatives', [
            new TableColumn('id_representative', ColumnTypes::BIGINT, 20, true, null, true, true),
            new TableColumn('id_clinic', ColumnTypes::BIGINT, 20),
            new TableColumn('name_representative', ColumnTypes::VARCHAR, 100),
            new TableColumn('description_representative', ColumnTypes::VARCHAR, 100),
            new TableColumn('type_representative', ColumnTypes::INTEGER, 11),
            new TableColumn('status_representative', ColumnTypes::BIT, 1, false, "b'1'")
        ]);
    }
}