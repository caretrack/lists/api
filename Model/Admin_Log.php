<?php


namespace Model;


class Admin_Log
{
    public function __construct()
    {
        $mysql = new MySQL();
        $mysql->create_table('_admin_log', [
            new TableColumn('id_admin_log', ColumnTypes::BIGINT, 20, true, null, true, true),
            new TableColumn('id_usuario', ColumnTypes::BIGINT, 20, true),
            new TableColumn('fecha', ColumnTypes::TIMESTAMP, 0, false, 'current_timestamp'),
            new TableColumn('mensaje', ColumnTypes::LONGBLOB, 0),
        ]);
    }

    public function insertMessage($log)
    {
        $sql = <<<sql
insert into `_admin_log` values(?,?,?,?);
sql;
        $mysql = new MySQL();
        $mysql->prepare($sql, [
            'iiss',
            null,//id_admin_log
            $log['id_usuario'],//id_usuario
            null,//fecha
            $log['mensaje'],//mensaje
        ]);
    }
}