<?php


namespace Model;


class Usuarios
{
    public function __construct()
    {
        $password_hash = password_hash('rasfull*', CRYPT_BLOWFISH);
        $mysql = new MySQL();
        $mysql->create_table('_usuarios', [
            new TableColumn('id_usuario', ColumnTypes::BIGINT, 20, true, null, true, true),
            new TableColumn('id_rep', ColumnTypes::BIGINT, 20),
            new TableColumn('nombre_usuario', ColumnTypes::VARCHAR, 100),
            new TableColumn('login_usuario', ColumnTypes::VARCHAR, 50, true),
            new TableColumn('password_usuario', ColumnTypes::VARCHAR, 255, true),
            new TableColumn('correo_usuario', ColumnTypes::VARCHAR, 255),
            new TableColumn('perfil_usuario', ColumnTypes::BIGINT, 20, true),
            new TableColumn('estatus_usuario', ColumnTypes::BIT, 1),
        ], <<<sql
insert into _usuarios(login_usuario,password_usuario,perfil_usuario) values('codeman','$password_hash',0);
sql
        );
    }

    public function selectUsuario(array $usuario)
    {
        $sql = <<<sql
select 
       id_usuario,
       nombre_usuario,
       login_usuario,
       password_usuario,
       correo_usuario,
       perfil_usuario,
       estatus_usuario
 from _usuarios where login_usuario=? or id_usuario=?;
sql;
        $mysql = new MySQL();
        return $mysql->fetch_single($mysql->prepare($sql, [
            'si',
            $usuario['login_usuario'],
            $usuario['id_usuario'],
        ]));
    }
}