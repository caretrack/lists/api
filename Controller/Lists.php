<?php


namespace Controller;

use HTTPStatusCodes;
use JsonResponse;
use Model\Pacientes;
use SimpleXLS;
use SimpleXLSX;
use Stopwatch;
use System;

class Lists
{
    /**
     * @var string
     */
    private $path;

    public function __construct()
    {
        $this->path = DIR . '/Data/';
    }

    /**
     * @apiDefine User
     * @apiParam {Object} user
     * @apiParam {String} user.token User Token.
     */
    /**
     * @apiDefine Patient
     * @apiSuccess {Object[]} patients Converted rows of patients.
     * @apiSuccess {String} patients.status
     * @apiSuccess {String} patients.ProviderLName
     * @apiSuccess {String} patients.ProviderNpi
     * @apiSuccess {String} patients.IPAName
     * @apiSuccess {String} patients.SubscriberNumber
     * @apiSuccess {String} patients.MemberLName
     * @apiSuccess {String} patients.MemberFName
     * @apiSuccess {String} patients.DateOfBirth
     * @apiSuccess {String} patients.MemberPhone
     * @apiSuccess {String} patients.Gender
     * @apiSuccess {String} patients.MemberEffectiveDate
     * @apiSuccess {String} patients.MemberAddress
     * @apiSuccess {String} patients.City
     * @apiSuccess {String} patients.State
     * @apiSuccess {String} patients.Zip
     * @apiSuccess {String} patients.County
     * @apiSuccess {String} patients.Language
     */

    /**
     * @apiVersion 0.1.0
     * @apiGroup Lists
     * @apiName uploadFile
     * @api {POST} /lists/uploadFile Upload CSV or Excel File
     *
     * @apiUse User
     * @apiParam {Object} FILES
     * @apiParam {FormData} FILES.list CSV or Excel File.
     * @apiParam {String} ipa IPA.
     * @apiParam {Object} period
     * @apiParam {Date} period.start List Period Start.
     * @apiParam {Date} period.end List Period End.
     *
     * @apiSuccess {Object} list
     * @apiSuccess {Int} list.id List ID
     * @apiSuccess {Object} totals
     * @apiSuccess {Int} totals.count Total of Members.
     * @apiSuccess {Int} totals.new Total of New Members.
     * @apiUse Patient
     * @apiError (Internal Server Error 500) FileNotMoved File could not be moved.
     * @apiError (Internal Server Error 500) FileNotOpened File could not be opened.
     */
    public function uploadFile()
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start();

        ['list' => $list] = $_FILES;
        ['user' => $user, 'ipa' => $ipa, 'period' => $period] = $_POST;
        ['token' => $token] = $user;

        System::check_value_empty(['period' => $period['start']], ['period'], 'Data missing.');
        $stopwatch->lap_end('check_value_empty');

        $path = $this->path . time() . "." . $list['name'];
        $user = System::decode_token($token);

        if (empty($list['tmp_name'])) {
            JsonResponse::sendResponse(['message' => 'Filename cannot be empty.'], HTTPStatusCodes::BadRequest);
        }

        if (!copy($list['tmp_name'], $path)) {
            JsonResponse::sendResponse(['message' => 'File could not be moved.'], HTTPStatusCodes::InternalServerError);
        }

        $info = pathinfo($path);

        $stopwatch->lap_end('validations');

        unset($list);
        $Lists = new \Model\Lists();
        $list['id'] = $Lists->saveFile($user['id_usuario'], [
            'filename' => $info['basename'],
            'ipa' => $ipa,
            'period_start' => $period['start'],
            'period_end' => $period['end'],
            'path' => $this->path
        ]);
        $stopwatch->lap_end('saveFile');

        list($parse, $patients, $totals) = $this->get_patient_totals($info, $list, $ipa, $period);

        $stopwatch->lap_end('get_patient_totals');

        $list['status'] = 'pending';

        $stopwatch->end('uploadFile');

        if ($parse) {
            return compact('list', 'totals', 'patients');
        } else {
            $error = !!$parse ? ${get_class($parse)}::parseError() : '';
            JsonResponse::sendResponse(['message' => "File could not be opened." . (!!$error ? "[$error]" : '')], HTTPStatusCodes::InternalServerError);
        }
        return null;
    }

    /**
     * @apiVersion 0.1.0
     * @apiGroup Lists
     * @apiName importPatients
     * @api {POST} /lists/importPatients Imports Patients from File.
     *
     * @apiParam {Object} list
     * @apiParam {Int} list.id List ID
     * @apiUse User
     *
     * @apiSuccess {String} message Completed
     */
    function importPatients()
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start();

        ['id' => $id] = $_POST['list'];

        ['token' => $token] = $_POST['user'];
        $user = System::decode_token($token);

        $Lists = new \Model\Lists();
        $list = $Lists->getList($id);
        $stopwatch->lap_end('getList');

        if (empty($list)) {
            JsonResponse::sendResponse(['message' => 'List not found.'], HTTPStatusCodes::BadRequest);
        }
        switch ($list['status']) {
            case "completed":
                JsonResponse::sendResponse(['message' => 'This list is already imported.'], HTTPStatusCodes::BadRequest);
                break;
        }

        $config = $this->get_ipa_config($list['ipa']);
        $stopwatch->lap_end('get_ipa_config');
        $patients = $this->get_patient_rows(pathinfo($this->path . $list['filename']), $list, $list['ipa'], $parse);
        $stopwatch->lap_end('get_patient_rows');
        $missing_patients = $this->get_missing_patients(['start' => $list['period_start']], $list['ipa'], $patients);
        $stopwatch->lap_end('get_missing_patients');

        $Pacientes = new Pacientes();
        $totals = ['count' => 0, 'new' => 0, 'unchanged' => 0, 'missing' => count($missing_patients)];
        $subscribernumerList = [];
        if (isset($list['period_start'])) {
            $subscribernumerList = $Pacientes->selectSubscriberNumbers($list['period_start']);
        }
        $stopwatch->lap_end('selectSubscriberNumbers');

        $new_patients = [];
        foreach ($patients as $patient) {
            $this->detect_status($subscribernumerList, $patient);
            $patient = [
                'id' => 0,
                'id_usuario' => $user['id_usuario'],
                'id_list' => $id,
                'fecha' => date('Y-m-d H:i:s'),
                'status' => $patient['status'],
                'ProviderLName' => addslashes($patient['ProviderLName']),
                'ProviderNpi' => $patient['ProviderNpi'],
                'IPAName' => $patient['IPAName'],
                'SubscriberNumber' => $patient['SubscriberNumber'],
                'MemberLName' => addslashes($patient['MemberLName']),
                'MemberFName' => addslashes($patient['MemberFName']),
                'DateOfBirth' => date('Y-m-d H:i:s', strtotime($patient['DateOfBirth'])),
                'MemberPhone' => $patient['MemberPhone'],
                'Gender' => $patient['Gender'],
                'MemberEffectiveDate' => date('Y-m-d H:i:s', strtotime($patient['MemberEffectiveDate'])),
                'MemberAddress' => $patient['MemberAddress'],
                'City' => $patient['City'],
                'State' => $patient['State'],
                'Zip' => $patient['Zip'],
                'County' => $patient['County'],
                'Language' => $patient['Language'],
            ];
            $new_patients[] = implode("','", array_values($patient));
            $totals[$patient['status']] += 1;
            $totals['count'] += 1;
        }
        $stopwatch->lap_end('foreach patients');

        $Pacientes->insertPacientes($new_patients);
        $stopwatch->lap_end('insert patients');

        $Lists->setListStatus($id, 'completed');
        $stopwatch->lap_end('setListStatus');

        $stopwatch->end('importPatients');

        return ['message' => "Completed.", 'totals' => $totals];
    }

    /**
     * @apiVersion 0.1.0
     * @apiGroup Lists
     * @apiName getLists
     * @api {POST} /lists/getLists Get All Totals from Lists.
     *
     * @apiUse User
     *
     * @apiSuccess {Object} totals
     * @apiSuccess {Object} period
     * @apiSuccess {Int} period.count
     * @apiSuccess {Int} period.new
     * @apiSuccess {Int} period.missing
     *
     * @apiSuccess {Object} lists
     * @apiSuccess {Object[]} lists.ipa
     * @apiSuccess {Int} lists.ipa.id
     * @apiSuccess {Object} lists.ipa.total
     * @apiSuccess {Int} lists.ipa.total.count
     * @apiSuccess {Int} lists.ipa.total.new
     * @apiSuccess {Int} lists.ipa.total.missing
     * @apiSuccess {Object} lists.ipa.period
     * @apiSuccess {Date} lists.ipa.period.start
     * @apiSuccess {Date} lists.ipa.period.end
     */
    function getLists()
    {
        ['user' => $user, 'period' => $period] = System::isset_get($_POST);
        ['token' => $token] = $user;
        System::decode_token($token);

        $Pacientes = new Pacientes();
        $patient_totals = $Pacientes->selectListTotals($period);
        $ipas = array_map(function ($file) {
            return pathinfo($file, PATHINFO_FILENAME);
        }, glob(DIR . '/Config/ipa/*.json'));
        $periods = [];
        $lists = [];
        foreach ($ipas as $ipa) {
            array_map(function ($key) use ($ipa, $patient_totals, &$lists, &$periods) {
                $list = $patient_totals[$key];

                $info = pathinfo($this->path . $list['filename']);
                $patients = $this->get_patient_rows($info, (array)$list, $ipa, $parse);
                $new_patients = $this->get_new_patients((array)['start' => $list['period_start']], $ipa, $patients);
                $missing_patients = $this->get_missing_patients((array)['start' => $list['period_start']], $ipa, $patients);

                $lists[0][$ipa][] = [
                    'id' => $list['id_list'],
                    'ipa' => $ipa,
                    'total' => $list['total'],
                    'total_extended' => [
                        'current' => $list['total'] - count($new_patients),
                        'new' => count($new_patients),
                        'missing' => count($missing_patients)
                    ],
                    'period' => ['start' => $list['period_start'], 'end' => $list['period_end']],
                    'data' => [
                        'date' => $list['date'],
                        'rep' => $list['rep'],
                        'status' => $list['status'],
                    ]
                ];
                $total = $list['total'];
                $periods[$list['period_start'] . ":" . $list['period_end']][$ipa] = [
                    'count' => $total,
                    'produced' => $total - count($missing_patients),
                    'current' => $total,
                    'new' => count($new_patients),
                    'missing' => count($missing_patients)
                ];
            }, array_keys(array_column($patient_totals, 'ipa'), $ipa));
        }

        $i = 1;
        $produced = [];
        $totals = [];
        foreach ($periods as $key => $column) {
            $period = ['start' => substr($key, 0, strpos($key, ':')), 'end' => substr(strstr($key, ':'), 1)];
            if (count($column) == count($ipas)) {
                $count = 0;
                $total = 0;
                $current = 0;
                $new = 0;
                $missing = 0;
                foreach ($column as $num => $values) {
                    $count += $values['count'];
                    $total += $values['produced'];
                    $current += $values['current'];
                    $new += $values['new'];
                    $missing += $values['missing'];
                }
                $totals[] = [
                    'id' => $i++,
                    'period' => $period,
                    'total' => $count,
                    'total_extended' => [
                        'current' => $current,
                        'new' => $new,
                        'missing' => $missing
                    ]
                ];
                $produced[] = [
                    'id' => $i,
                    'period' => $period,
                    'total' => $new - $missing
                ];
            } else {
                $totals[] = [
                    'id' => $i++,
                    'period' => $period
                ];
            }
        }
        return compact('produced', 'totals', 'lists', 'ipas');
    }

    function deleteList()
    {
        ['user' => $user, 'list' => $list] = $_POST;
        ['id' => $id] = $list;

        ['token' => $token] = $user;
        System::decode_token($token);

        $Lists = new \Model\Lists();
        $Lists->updateStatus(System::isset_get($id, 0), 'deleted');

        $Paciente = new Pacientes();
        $Paciente->deletePatientsFromList($id);

        $Lists->deleteList($id);

        return 'Completed.';
    }

    /**
     * @param $ipa
     * @return array
     */
    private function get_ipa_config($ipa)
    {
        if (!file_exists(DIR . '/Config/ipa/' . $ipa . '.json')) {
            JsonResponse::sendResponse(['message' => "Config for $ipa not found."], HTTPStatusCodes::BadRequest);
        }
        return json_decode(file_get_contents(DIR . '/Config/ipa/' . $ipa . '.json'), true);
    }

    /**
     * @param array $info
     * @param array $list
     * @param string $ipa
     * @param boolean $parse
     * @return array
     */
    private function get_patient_rows(array $info, array $list, $ipa, &$parse)
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start();

        $path = $info['dirname'] . '/' . $info['basename'];
        switch ($info['extension']) {
            case 'csv':
                $row = 0;
                if (($handle = fopen($path, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $rows[] = $data;
                        $parse = true;
                        $row++;
                    }
                    fclose($handle);
                }
                break;
            case 'xls':
                $parse = SimpleXLS::parse($path);
                $rows = $parse->rows();
                break;
            case 'xlsx':
                $parse = SimpleXLSX::parse($path);
                if (!$parse) {
                    return [];
                }
                $rows = $parse->rows();
                break;
            default:
                JsonResponse::sendResponse(['message' => 'Not accepted file extension.']);
                break;
        }
        $stopwatch->lap_end('parse_file');

        $config = $this->get_ipa_config($ipa);
        if ($config['headers'] !== $rows[0]) {
            JsonResponse::sendResponse(['message' => "Config for $ipa is not valid for this file."], HTTPStatusCodes::BadRequest);
        }
        $stopwatch->lap_end('get_ipa_config');

        $patients = [];
        $List = new \Model\Lists();
        $list = $List->getList(System::isset_get($list['id']));
        $stopwatch->lap_end('getList');

        $subscribernumerList = [];
        if (isset($list['period_start'])) {
            $Pacientes = new Pacientes();
            $subscribernumerList = $Pacientes->selectSubscriberNumbers($list['period_start']);
        }
        foreach ($rows as $index => $row) {
            if ($index === 0) {
                continue;
            }
            $patient = array_combine($config['headers'], (array)$row);
            $this->detect_status($subscribernumerList, $patient);
            array_push($patients, $patient);
        }
        $stopwatch->lap_end('foreach_rows');

        $stopwatch->end('get_patient_rows');
        return $patients;
    }

    /**
     * @param array $period
     * @param string $ipa
     * @param array $patients
     * @return array
     */
    private function get_new_patients(array $period, $ipa, array $patients)
    {
        $Lists = new \Model\Lists();
        $previous = $Lists->getPreviousList($period['start'], $ipa);
        if (!empty($previous['filename'])) {
            $last_patients = $this->get_patient_rows(pathinfo($this->path . $previous['filename']), $previous, $ipa, $parse);
            foreach ($patients as $aV) {
                $aTmp1[] = $aV['SubscriberNumber'];
            }
            foreach ($last_patients as $aV) {
                $aTmp2[] = $aV['SubscriberNumber'];
            }
            return array_diff((array)System::isset_get($aTmp1, []), (array)System::isset_get($aTmp2, []));
        }
        return [];
    }

    /**
     * @param array $period
     * @param string $ipa
     * @param array $patients
     * @return array
     */
    private function get_missing_patients(array $period, $ipa, $patients)
    {
        $Lists = new \Model\Lists();
        $previous = $Lists->getPreviousList($period['start'], $ipa);
        if (!empty($previous['filename'])) {
            $last_patients = $this->get_patient_rows(pathinfo($this->path . $previous['filename']), $previous, $ipa, $parse);
            foreach ($patients as $aV) {
                $aTmp1[] = $aV['SubscriberNumber'];
            }
            foreach ($last_patients as $aV) {
                $aTmp2[] = $aV['SubscriberNumber'];
            }
            $missing = array_diff((array)System::isset_get($aTmp2, []), (array)System::isset_get($aTmp1, []));

            foreach ($missing as $key => $item) {
                $last_patients[$key]['status'] = 'missing';
                array_push($patients, $last_patients[$key]);
            }

            return $missing;
        }
        return [];
    }

    /**
     * @param array $subscribernumber_list
     * @param array $patient
     * @return array
     */
    private function detect_status(array $subscribernumber_list, array &$patient)
    {
        $list = array_flip(array_column($subscribernumber_list, 'SubscriberNumber'));
        $SubscriberNumber = $patient['SubscriberNumber'];
        $paciente = isset($list[$SubscriberNumber]);
        switch (true) {
            case empty($paciente):
                $status = 'new';
                break;
            case !!System::isset_get($patient['status']):
                $status = $patient['status'];
                break;
            default:
                $status = 'unchanged';
                break;
        }
        $patient = ['status' => $status] + $patient;
        return $patient;
    }

    /**
     * @param $info
     * @param $list
     * @param $ipa
     * @param $period
     * @return array
     */
    private function get_patient_totals($info, $list, $ipa, $period)
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start();

        $patients = $this->get_patient_rows($info, (array)$list, $ipa, $parse);
        $stopwatch->lap_end('get_patient_rows');
        $new_patients = $this->get_new_patients((array)$period, $ipa, $patients);
        $stopwatch->lap_end('get_new_patients');
        $missing_patients = $this->get_missing_patients((array)$period, $ipa, $patients);
        $stopwatch->lap_end('get_missing_patients');

        $totals = [
            'count' => count($patients),
            'new' => count($new_patients),
            'missing' => count($missing_patients),
        ];

        $stopwatch->end('get_patient_totals');

        return array($parse, $patients, $totals);
    }

}