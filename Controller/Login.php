<?php


namespace Controller;


use JsonResponse;
use Model\Admin_Log;
use Model\Usuarios;
use System;

class Login
{
    /**
     * @apiDefine User
     * @apiParam {Object} user User.
     * @apiParam {string} user.token User Token.
     */

    /**
     * @apiDefine Usuario
     * @apiSuccess {Object} usuario User.
     * @apiSuccess {Int} usuario.id User ID.
     * @apiSuccess {String} usuario.nombre User Name.
     * @apiSuccess {String} usuario.login User Login.
     * @apiSuccess {String} usuario.correo User Email.
     * @apiSuccess {Int} usuario.perfil User Access Level.
     * @apiSuccess {String} usuario.token User Token.
     * @apiSuccess {Boolean} usuario.logged User is logged?
     */

    /**
     * @apiVersion 0.1.0
     * @apiGroup Login
     * @apiName iniciarSesion
     * @param $user
     * @return array
     * @api {post} /login/iniciarSesion Log In
     *
     * @apiParam {String} login User Login.
     * @apiParam {String} password User Password.
     *
     * @apiSuccess {String} token User Token.
     * @apiError (Bad Request 400) WrongData Wrong data.
     */
    function iniciarSesion($user = [])
    {
        if (empty($user)) {
            $user = [
                'login_usuario' => System::isset_get($_POST['login']),
                'password_usuario' => System::isset_get($_POST['password']),
            ];
        }
        $Usuarios = new Usuarios();
        $usuario = $Usuarios->selectUsuario($user);

        if (empty($usuario)) {
            JsonResponse::sendResponse(['message' => 'Wrong data.']);
        }

        if (!empty($user['password_usuario']) || empty($user['id_usuario'])) {
            if (!password_verify($user['password_usuario'], $usuario['password_usuario'])) {
                JsonResponse::sendResponse(['message' => 'Wrong data.']);
            }
        }

        $Admin_Log = new Admin_Log();
        $Admin_Log->insertMessage(['mensaje' => 'Logged In', 'id_usuario' => $usuario['id_usuario']]);

        $token = System::encode_token($usuario);

        return compact('token');
    }

    /**
     * @apiVersion 0.1.0
     * @apiGroup Login
     * @apiName validarUsuario
     * @api {post} /login/validarUsuario Validate User
     *
     * @apiUse User
     *
     * @apiUse Usuario
     *
     * @apiError (Bad Request 400) ExpiredToken Expired Token
     * @apiError (Bad Request 400) InvalidToken Invalid Token
     */
    function validarUsuario()
    {
        $user = [
            'token' => System::isset_get($_POST['user']['token']),
        ];
        $usuario = System::decode_token($user['token']);
        return [
            'usuario' => [
                'id' => $usuario['id_usuario'],
                'nombre' => $usuario['nombre_usuario'],
                'login' => $usuario['login_usuario'],
                'correo' => $usuario['correo_usuario'],
                'perfil' => $usuario['perfil_usuario'],
                'token' => $user['token'],
                'logged' => true
            ]
        ];
    }
}
