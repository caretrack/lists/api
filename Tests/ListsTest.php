<?php
declare(strict_types=1);

use Controller\Lists;
use Controller\Login;
use Model\MySQL;
use PHPUnit\Framework\TestCase;

class ListsTest extends TestCase
{
    public function __construct()
    {
        parent::__construct();

        require_once __DIR__ . "/../Lib/System.php";
        require_once __DIR__ . "/../Model/Admin_Log.php";
        require_once __DIR__ . "/../Model/Usuarios.php";
        require_once __DIR__ . "/../Controller/Login.php";
        require_once __DIR__ . "/../Model/Representatives.php";
        require_once __DIR__ . "/../Model/Pacientes.php";
        require_once __DIR__ . "/../Model/Lists.php";
        require_once __DIR__ . "/../Controller/Lists.php";

        $system = new System();
        $system->init(['DIR' => __DIR__ . '/..']);
    }

    function testUploadFilesVantage()
    {
        $token = $this->getToken();
        $this->reset();

        $_POST = [
            'user' => [
                'token' => $token
            ],
            'ipa' => 'vantage',
        ];

        $this->assertTotals(
            '19 05 Vantage.xlsx',
            [
                'start' => '2019-05-01',
                'end' => '2019-05-30'
            ],
            [
                'count' => 10,
                'new' => 0,
                'missing' => 0,
            ]
        );

        $this->assertTotals(
            '19 06 Vantage.xlsx',
            [
                'start' => '2019-06-01',
                'end' => '2019-06-30'
            ],
            [
                'count' => 12,
                'new' => 2,
                'missing' => 0,
            ]
        );

        $this->assertTotals(
            '19 07 Vantage.xlsx',
            [
                'start' => '2019-07-01',
                'end' => '2019-07-30'
            ],
            [
                'count' => 11,
                'new' => 1,
                'missing' => 2,
            ]
        );

        $this->assertTotals(
            '19 08 Vantage.xlsx',
            [
                'start' => '2019-08-01',
                'end' => '2019-08-30'
            ],
            [
                'count' => 13,
                'new' => 2,
                'missing' => 0,
            ]
        );
    }

    function testImportPatientsVantage()
    {
        $this->testUploadFilesVantage();

        $Lists = new Lists();
        $_POST['list']['id'] = 1;
        $response = $Lists->importPatients();
        $this->assertEquals([
            'count' => 10,
            'unchanged' => 0,
            'new' => 10,
            'missing' => 0,
        ], $response['totals']);

        $_POST['list']['id'] = 2;
        $response = $Lists->importPatients();
        $this->assertEquals([
            'count' => 12,
            'unchanged' => 10,
            'new' => 2,
            'missing' => 0,
        ], $response['totals']);


        $_POST['list']['id'] = 3;
        $response = $Lists->importPatients();
        $this->assertEquals([
            'count' => 11,
            'unchanged' => 10,
            'new' => 1,
            'missing' => 2,
        ], $response['totals']);

        $_POST['list']['id'] = 4;
        $response = $Lists->importPatients();
        $this->assertEquals([
            'count' => 13,
            'unchanged' => 11,
            'new' => 2,
            'missing' => 0,
        ], $response['totals']);
    }

    function testGetLists()
    {
        $this->testImportPatientsVantage();
        $token = $this->getToken();
        $_POST = [
            'user' => [
                'token' => $token
            ],
            'period' => [
                'start' => '2019-05-01',
                'end' => '2019-10-31'
            ]
        ];
        $Lists = new Lists();
        $response = $Lists->getLists();

        $lists = $response['lists'][0]['vantage'];
        $this->assertEquals(['current' => 10, 'new' => 0, 'missing' => 0], $lists[0]['total_extended']);
        $this->assertEquals(['current' => 10, 'new' => 2, 'missing' => 0], $lists[1]['total_extended']);
        $this->assertEquals(['current' => 10, 'new' => 1, 'missing' => 2], $lists[2]['total_extended']);
        $this->assertEquals(['current' => 11, 'new' => 2, 'missing' => 0], $lists[3]['total_extended']);

        $this->reset();
    }

    function testBigFile()
    {
        $stopwatch = new Stopwatch();
        $token = $this->getToken();
        $this->reset();
        $_POST = [
            'user' => [
                'token' => $token
            ],
            'ipa' => 'vantage',
        ];
        $filename = 'XAssignedRoster.csv';
        $period = [
            'start' => '2019-05-01',
            'end' => '2019-05-30',
        ];
        $Lists = new Lists();
        $_FILES['list'] = [
            'name' => $filename,
            'tmp_name' => getcwd() . "/Data/" . $filename,
        ];
        $_POST['period'] = $period;

        $stopwatch->start();

        $responseUploadFile = $Lists->uploadFile();
        $stopwatch->lap_end('time_upload_file');

        $_POST['list']['id'] = 1;
        $responseImportPatients = $Lists->importPatients();
        $stopwatch->lap_end('time_import_patients');

        $stopwatch->end('testBigFile');

        self::assertEquals(6863, $responseUploadFile['totals']['count']);
        self::assertEquals($responseUploadFile['totals']['count'], $responseImportPatients['totals']['count']);

        $report = $stopwatch->report();
        $stopwatch->report('importPatients');
        $stopwatch->report('testBigFile');
        $laps = $report['testBigFile']->measure_points;
        self::assertEqualsWithDelta(1, $laps['time_upload_file'], 0.5);
        self::assertEqualsWithDelta(4, $laps['time_import_patients'], 0.5);
        self::assertEquals(5,$laps['time_upload_file'] + $laps['time_import_patients']);

        $this->reset();
    }

    function testAllowedMemory()
    {
        $stopwatch = new Stopwatch();
        $Lists = new Lists();
        $filename = 'XAssignedRoster.xlsx';
        $months = [5, 10];
        $token = $this->getToken();
        $this->reset();

        $stopwatch->start();
        for ($i = $months[0]; $i <= $months[1]; $i++) {
            $_POST['user']['token'] = $token;
            $_POST['ipa'] = 'vantage';
            $_FILES['list']['name'] = $filename;
            $_FILES['list']['tmp_name'] = getcwd() . "/Data/" . $filename;
            $_POST['period']['start'] = "2019-$i-01";
            $_POST['period']['end'] = "2019-$i-01";
            $response = $Lists->uploadFile();
            self::assertEquals(6863, $response['totals']['count']);
            self::assertEquals($response['totals']['count'], count($response['patients']));
        }

        for ($i = 1; $i <= ($months[1] - $months[0]) + 1; $i++) {
            $_POST['list']['id'] = $i;
            $_POST['user']['token'] = $token;
            $response = $Lists->importPatients();
            self::assertEquals(6863, $response['totals']['count']);
        }
        $stopwatch->end('testAllowedMemory');
        $report = $stopwatch->report();

        self::assertLessThan(30, $report['importPatients']->measure_points['get_patient_rows']);
        self::assertLessThan(30, $report['importPatients']->measure_points['foreach']);

        self::assertLessThan(90, $stopwatch->report('testAllowedMemory'));
    }

    private function getToken()
    {
        $_POST = [
            'login' => 'codeman',
            'password' => 'rasfull*'
        ];
        $Login = new Login();
        return $Login->iniciarSesion()['token'];
    }

    private function assertTotals(string $filename, array $period, array $totals)
    {
        $Lists = new Lists();
        $_FILES['list'] = [
            'name' => $filename,
            'tmp_name' => getcwd() . "/Data/" . $filename,
        ];
        $_POST['period'] = $period;
        $response = $Lists->uploadFile();
        $this->assertEquals($totals, $response['totals']);
    }

    private function reset()
    {
        $mysql = new MySQL();
        $mysql->delete_tables(['lists', 'pacientes']);

        $files = glob(getcwd() . "/../Data/*.xlsx"); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
        }
        $files = glob(getcwd() . "/../Data/*.csv"); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
        }
    }
}